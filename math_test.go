package main

import (
	"testing"
)

// TestTryRounding
func TestTryRounding(t *testing.T) {
	var i []int
	k := []int{12, 13, 131, 259}
	result := tryRounding(i, k)
	r := []int{10, 15, 130, 260}
	
	if len(result) != len(r){
		t.Errorf("Result set was not the expected size. Expected %d, got %d", len(result), len(r))
	}

	for i := range r {
        if result[i] != r[i] {
            t.Errorf("result[%d] doesnt match r[%d]. Expected %d, got %d", i, i, result[i], r[i])
        }
    }
}

// TestroundInt tests the roundInt func
func TestRoundToInt(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "success round down",
			args: args{
				x: 12,
				y: 5,
			},
			want: 10,
		},
		{
			name: "success round up",
			args: args{
				x: 13,
				y: 5,
			},
			want: 15,
		},
		{
			name: "success round down large",
			args: args{
				x: 131,
				y: 5,
			},
			want: 130,
		},
		{
			name: "success round up large",
			args: args{
				x: 259,
				y: 5,
			},
			want: 260,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := roundToInt(tt.args.x, tt.args.y); got != tt.want {
				t.Errorf("roundToInt() = %v, want %v", got, tt.want)
			}
		})
	}
}
