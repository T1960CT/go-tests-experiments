package main

import (
	"fmt"
)

func giveAnswer(num int, res int) string {
	return fmt.Sprintf("For input %d, rounding to nearest 5, we get %d", num, res)
}