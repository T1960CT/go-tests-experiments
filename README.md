# Go-Tests-Experiments
This project was created in order to experiment and understand Go testing, code coverage, generating badges, integration with `coveralls.io`, and more.

[![Coverage Status](https://coveralls.io/repos/gitlab/T1960CT/go-tests-experiments/badge.svg)](https://coveralls.io/gitlab/T1960CT/go-tests-experiments)