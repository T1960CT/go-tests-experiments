package main

import (
	"fmt"
)

func main() {
	
	getValues()
}

var err error
func getValues(){
	var income, frequency, incomeSplit int

	fmt.Print("What is your monthly income: ")
	_, err = fmt.Scan(&income)

	fmt.Println("(weekly = 4, biweekly = 2, monthly = 1)")
	fmt.Print("What is your many times per mo. are you paid: ")
	_, err = fmt.Scan(&frequency)

	var rent, phone, electricity, food, outings, pets, credit, unknown, misc, expenses, expensesSplit int

	fmt.Print("What is your monthly rent: ")
	_, err = fmt.Scan(&rent)

	fmt.Print("What is your monthly phone bill: ")
	_, err = fmt.Scan(&phone)

	fmt.Print("What is your monthly electricity bill: ")
	_, err = fmt.Scan(&electricity)

	fmt.Print("What is your monthly food bill: ")
	_, err = fmt.Scan(&food)

	fmt.Print("What is your monthly outings norm: ")
	_, err = fmt.Scan(&outings)

	fmt.Print("What is your monthly pet bill: ")
	_, err = fmt.Scan(&pets)

	fmt.Print("What is your monthly credit payment: ")
	_, err = fmt.Scan(&credit)

	fmt.Println("payments such as netflix, spotify, etc.")
	fmt.Print("What is your monthly miscellaneous total: ")
	_, err = fmt.Scan(&misc)

	fmt.Print("What is your monthly 'unexpected' padding: ")
	_, err = fmt.Scan(&unknown)

	expenses = rent+phone+electricity+food+outings+pets+credit+misc+unknown
	expensesSplit = expenses/frequency
	incomeSplit = income/frequency

	fmt.Printf("Expenses %d, Split %d, Income %d, Split %d, Frequency %d",expenses, expensesSplit, income, incomeSplit, frequency)
}

/*
	r= 1000
	p= 100
	h= 50
	f= 200
	o= 200
	u= 100

	%= 75

	i= 2400
	x=2

	r+p+h+f+o = 1650
	i = 		2400
	x =			   2

	bh = 1550/2 = 825
	ih = 2400/2 = 1200

	sx = (375 * 75) / 100 = 281.25
	st = 562.5
	sy = $6750
*/