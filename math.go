package main

import (
	"fmt"
)

func tryRounding(i []int, k []int) []int {
	for n := 0; n < len(k); n++ {
		i = append(i, n)
		i[n] = roundToInt(k[n], 5)
		fmt.Println(giveAnswer(k[n], i[n]))
	}
	return i
}


// roundToInt takes int n and rounds it using precision p, returning the rounded int.
// A.K.A. round n to nearest p
func roundToInt(n int, p int) int {

	// if n below mid point
	var a = (int)(n/p) * p

	// if n at or above mid point
	var b = (a + p)

	// check to see which
	if n-a > b-n {
		return b
	}
	return a
}

/*
	Test math:
		// test 1
		n=12, p=5
		a = (int)(12 / 5) * 5
		a = (int)2.4 * 5
		a = 2 * 5
		a = 10

		b = 10 + 5
		b = 15

		if 12-10 > 15-12
		if 2 > 3 (not), return 10

		// test 2
		n=13, p=5
		a = (int)(13 / 5) * 5
		a = (int)2.6 * 5
		a = 2 * 5
		a = 10

		b = 10 + 5
		b = 15

		if 13-10 > 15-13
		if 3 > 2 (yes), return 15
*/
