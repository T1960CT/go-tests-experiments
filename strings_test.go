package main

import (
	"testing"
)

func TestGiveAnswer(t *testing.T) {
	result := giveAnswer(12, 10)
    if result != "For input 12, rounding to nearest 5, we get 10" {
       t.Errorf("Printed Incorrectly.")
    }
}
